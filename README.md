# Carrito de compras

__Tecnologías usadas:__

+ Laravel 5.5
+ Paypal SKD beta-2.0
+ Vue.js
+ SASS
+ Bootstrap

__Previews:__

![](preview-home.png)
![](preview-carrito.png)